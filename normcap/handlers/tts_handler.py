# Default
import os

# Extra
import pyttsx3

# Own
from normcap.common.data_model import NormcapData
from normcap.handlers.abstract_handler import AbstractHandler


class TTSHandler(AbstractHandler):
    def handle(self, request: NormcapData) -> NormcapData:
        """Trigger system notification when ocr is done.

        Arguments:
            AbstractHandler {class} -- self
            request {NormcapData} -- NormCap's session data

        Returns:
            NormcapData -- Enriched NormCap's session data
        """
        self._logger.info("Sending notification...")
        self._verbose = request.cli_args["verbose"]
        if request.cli_args["enable_tts"]:
            text = request.transformed.replace(os.linesep, " ")
            self._logger.info("Starting TTS...")

            if not request.test_mode:
                engine = pyttsx3.init()
                engine.say(text)
                engine.runAndWait()

        if self._next_handler:
            return super().handle(request)

        return request
